;;;; Put your name and section here

(defn qextends?
  "Returns true if a queen at rank extends partial-sol."
  [partial-sol rank]
    nil)
 
(defn qextend
  "Given a vector *partial-sol-list* of all partial solutions of length k,
  returns a vector of all partial solutions of length k + 1."
  [n partial-sol-list]
    nil)
  
(defn sol-count
  "Returns the total number of n-queens solutions on an n x n board."
  [n]
    nil)
  
(defn sol-density
  "Return the density of solutions on an n x n board."
  [n]
    nil)
